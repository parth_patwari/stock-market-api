package com.virtusa.training.stockmarketapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.stream.Collector

@RestController
@RequestMapping("/stocks")
class StockCtrl {

    @Autowired
    lateinit var stockService: StockService

    @GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getStocks(): Mono<Map<String, Stock>> {
        return Mono.just(stockService.getStockData())
    }

    @GetMapping("/feed", produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
    fun getChangeFeed(): Flux<Map<String, Stock>> {
        return stockService.getChanges()
    }
}