package com.virtusa.training.stockmarketapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class StockMarketApiApplication

fun main(args: Array<String>) {
	runApplication<StockMarketApiApplication>(*args)
}
