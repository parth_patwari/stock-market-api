package com.virtusa.training.stockmarketapi

import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.FluxProcessor
import reactor.core.publisher.FluxSink
import reactor.core.publisher.ReplayProcessor
import reactor.core.scheduler.Scheduler
import reactor.core.scheduler.Schedulers
import java.math.BigDecimal
import java.nio.file.Files
import java.nio.file.Path
import java.time.Duration
import java.util.stream.IntStream
import kotlin.random.Random

@Service
class StockService {

    var stockMap: Map<String, Stock> = mutableMapOf()
    var keys: List<String> = mutableListOf()
    val changeProcessor: FluxProcessor<Map<String, Stock>, Map<String, Stock>> = ReplayProcessor.create()
    val changeSink: FluxSink<Map<String, Stock>> = changeProcessor.sink()

    init {
        val readAllLines =
                Files.readAllLines(Path.of(this.javaClass.getResource("/stocks.csv").toURI()))
        readAllLines
                .stream()
                .filter({ item -> !item.contains("Symbol,Name,Last Sale") })
                .map { item: String ->
                    val stockData = item.split(",").toTypedArray()
                    Stock(
                            stockData[0],
                            stockData[1],
                            BigDecimal(stockData[2].trim().replace("$", "")),
                            BigDecimal(stockData[3].trim()),
                            BigDecimal(stockData[4].trim().replace("%", "")),
                            stockData[5].trim().replace("%", "").toLong(),
                            stockData[7],
                            stockData[8].trim().replace("%", "").toLong(),
                            stockData[9],
                            stockData[10]
                    )
                }.forEach {
                    stockMap = stockMap.plus(it.symbol to it)
                    keys = keys.plus(it.symbol)
                }


        Flux.interval(Duration.ofSeconds(3))
                .doOnEach { item ->
                    //println("Ticker " + item?.get())
                    var changes: Map<String, Stock> = mutableMapOf()
                    IntStream
                            .range(0, (Random.nextInt(stockMap.size) % 5))
                            .forEach { index ->
                                val key = keys.get(index)
                                val stock = stockMap.get(key)
                                stockMap = stockMap.plus(
                                        key to stock!!.change()
                                )
                                changes = changes.plus(key to stock)
                            }

                    println("Changes" + changes)
                    changeSink.next(changes)
                }.subscribe()
    }

    fun getStockData(): Map<String, Stock> {
        return stockMap
    }

    fun getChanges(): Flux<Map<String, Stock>> {
        return changeProcessor
    }
}